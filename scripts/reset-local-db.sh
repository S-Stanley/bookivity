#!/bin/bash

docker-compose stop postgraphile

docker-compose exec db sh -c "cd /database/ && psql -U postgres --quiet -f setup.sql"

docker-compose up postgraphile -d