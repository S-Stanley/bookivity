DROP DATABASE IF EXISTS dev;

CREATE DATABASE dev;

\c dev

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

\i seeds/tables.sql

\i fixtures/activities.sql
\i fixtures/cities.sql
\i fixtures/hosts.sql
\i fixtures/hosts_activities.sql

\i seeds/functions/search_activity.sql
\i seeds/functions/create_or_get_user.sql