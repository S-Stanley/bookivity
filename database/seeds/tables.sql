CREATE TABLE public.users(
    id          UUID PRIMARY KEY UNIQUE DEFAULT uuid_generate_v4(),
    email       VARCHAR(255) UNIQUE NOT NULL,
    firstname   VARCHAR(255) NOT NULL,
    familyname  VARCHAR(255) NOT NULL,
    token       UUID UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    created_at  DATE NOT NULL DEFAULT NOW()
);

CREATE TABLE public.cities (
    id          UUID UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    name        VARCHAR(255) UNIQUE NOT NULL
);

CREATE TABLE public.hosts (
    id                  UUID PRIMARY KEY UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    name                VARCHAR(255) NOT NULL,
    address             VARCHAR(255),
    site_web            VARCHAR(255),
    city_id             UUID NOT NULL,
    created_at          DATE NOT NULL DEFAULT NOW(),

    CONSTRAINT  fk_city FOREIGN KEY (city_id) REFERENCES public.cities (id)
);

CREATE TABLE public.activities (
    id                  UUID PRIMARY KEY UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    name                VARCHAR(100),
    created_at          DATE NOT NULL DEFAULT NOW()
);

CREATE TABLE public.events (
    id                  UUID PRIMARY KEY UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    happened_at         DATE DEFAULT NULL,
    uri_invitation      UUID UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    email_creator       VARCHAR(255) NOT NULL,
    firstname_creator   VARCHAR(255) NOT NULL,
    created_at          DATE NOT NULL DEFAULT NOW()
);

CREATE TABLE public.participations (
    id                  UUID PRIMARY KEY UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    email               UUID NOT NULL,
    firstname           VARCHAR(255) NOT NULL,
    event_id            UUID NOT NULL,
    created_at          DATE NOT NULL DEFAULT NOW(),

    CONSTRAINT  fk_event_id FOREIGN KEY (event_id) REFERENCES public.events (id)
);

CREATE TABLE public.hosts_activities (
    id                  UUID PRIMARY KEY UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    host_id             UUID NOT NULL,
    activity_id         UUID NOT NULL,
    picture_url         VARCHAR(255),
    description         TEXT,
    created_at          DATE NOT NULL DEFAULT NOW(),

    CONSTRAINT  fk_host_id FOREIGN KEY (host_id) REFERENCES public.hosts (id),
    CONSTRAINT  fk_activity_id FOREIGN KEY (activity_id) REFERENCES public.activities (id)
);