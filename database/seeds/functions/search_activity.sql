CREATE OR REPLACE FUNCTION public.search_activity (
    _city_id UUID,
    _activity_id UUID,
    OUT host_id UUID,
    OUT host_name TEXT,
    OUT host_address TEXT,
    OUT picture_url TEXT,
    OUT description TEXT,
    OUT host_activity_id UUID
) RETURNS SETOF RECORD AS $$
    SELECT
        DISTINCT(hosts.id) AS host_id,
        hosts.name AS host_name,
        hosts.address AS host_address,
        hosts_activities.picture_url AS picture_url,
        hosts_activities.description AS description,
        hosts_activities.id AS host_activity_id
    FROM
        hosts_activities
    JOIN
        public.hosts
    ON
        hosts_activities.host_id = hosts.id
    JOIN
        public.activities
    ON
        hosts_activities.activity_id = activities.id
    WHERE
        hosts.city_id = _city_id
    AND
        activities.id = _activity_id
$$ LANGUAGE SQL SECURITY INVOKER;