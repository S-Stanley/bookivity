CREATE OR REPLACE FUNCTION public.create_or_get_user(
    user_email      TEXT,
    user_firstname  TEXT,
    user_familyname TEXT
) RETURNS public.users AS $$
DECLARE
    _user_to_found public.users DEFAULT NULL;
BEGIN

    SELECT
        id,
        email,
        firstname,
        familyname,
        token
    INTO
        _user_to_found
    FROM
        public.users
    WHERE
        email = user_email;

    IF (_user_to_found IS NULL) THEN
        INSERT INTO
            public.users (
                email,
                firstname,
                familyname
            )
        VALUES (
            user_email,
            user_firstname,
            user_familyname
        )
        RETURNING
            id,
            email,
            firstname,
            familyname,
            token
        INTO
            _user_to_found;
    END IF;

    RETURN _user_to_found;

END;
$$ LANGUAGE PLPGSQL SECURITY INVOKER;