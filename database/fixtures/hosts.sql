TRUNCATE TABLE public.hosts CASCADE;

INSERT INTO public.hosts (
    id,
    name,
    address,
    site_web,
    city_id
)
VALUES (
    'fa0a62c4-8d5f-4457-9f2b-165537826a1c',
    'host-0001',
    '4 rue des hibiscus, 97233 Schoelcher',
    'https://host-0001.com',
    'e121e02a-7c0d-4744-a307-35d903d50176' --schoelcher
);