TRUNCATE TABLE public.activities CASCADE;

INSERT INTO public.activities (
    id,
    name
)
VALUES(
    '780929b1-2beb-4741-a0ec-d651e212d7fc',
    'Tennis'
), (
    'a05a07f7-c602-4c23-8f45-c0760a4c133d',
    'Kayak'
);