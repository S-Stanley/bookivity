TRUNCATE public.cities CASCADE;

INSERT INTO public.cities (
    id,
    name
)
VALUES (
    'e121e02a-7c0d-4744-a307-35d903d50176',
    'Schoelcher'
), (
    'e079a39a-0f3c-4b0b-bdd5-9bef9062c0b7',
    'Fort-de-France'
);