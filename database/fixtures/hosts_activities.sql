TRUNCATE TABLE public.hosts_activities CASCADE;

INSERT INTO public.hosts_activities(
    id,
    host_id,
    activity_id,
    picture_url,
    description
)
VALUES (
    '8108eba8-e878-432b-b6e5-378c7f83fbbd',
    'fa0a62c4-8d5f-4457-9f2b-165537826a1c', -- host-001
    '780929b1-2beb-4741-a0ec-d651e212d7fc', -- tennis
    'https://cdn.pixabay.com/photo/2021/06/04/06/54/racket-6308994_1280.jpg',
    'Venez profiter de ce terrain de tennis hyper cool'
), (
    '5017e335-dfc0-4517-94c6-8b68c43de56b',
    'fa0a62c4-8d5f-4457-9f2b-165537826a1c', -- host-001
    'a05a07f7-c602-4c23-8f45-c0760a4c133d', --kayak
    'https://cdn.pixabay.com/photo/2017/03/27/14/53/canoe-2179196_1280.jpg',
    'Faites une balade en mer en kayak et profitez du paysage'
);