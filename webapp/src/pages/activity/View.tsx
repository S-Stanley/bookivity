import { gql, useQuery } from "@apollo/client";
import { Button, Typography } from "@mui/material";
import { useParams } from "react-router-dom";

import './View.css';

const ActivityView = () => {

    const params = useParams();

    const { data, loading } = useQuery(
        gql`
            query getDataActivityView(
                $hostId: UUID!
            ) {
                hostsActivityById(id: $hostId) {
                    activityId
                    createdAt
                    hostId
                    id
                    pictureUrl
                    description
                    hostByHostId {
                        address
                        cityId
                        id
                        name
                        nodeId
                        siteWeb
                    }
                }
            }
        `,
        {
            variables: {
                hostId: params.host_activity_id
            }
        }
    )

    if (loading) {
        return (<div>Loading</div>);
    }

    return (
        <div className="body-page-view-activity">
            <div>
                <Typography variant="h2" className="item-view-activity">{data?.hostsActivityById?.hostByHostId?.name}</Typography>
                <Typography variant="h4" className="item-view-activity">{data?.hostsActivityById?.hostByHostId?.address}</Typography>
            </div>
            <div className="item-view-activity">
                <img
                    src={data?.hostsActivityById?.pictureUrl}
                    width='500px'
                    alt='activity'
                />
            </div>
            <div className="item-view-activity">
                <Typography>{data?.hostsActivityById?.description}</Typography>
            </div>
            <div className="item-view-activity">
                <Button variant="contained">
                    Créer l'evenement
                </Button>
            </div>
        </div>
    )
};

export default ActivityView;