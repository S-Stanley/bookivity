import { Grid, Typography, ButtonBase, Button } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import './SearchList.css';

const SearchList = (props: {
    activity: {
        hostId: string,
        hostName: string,
        hostAddress: string,
        pictureUrl: string,
        description: string,
        hostActivityId: string,
    }
}) => {

    const navigate = useNavigate();

    return (
        <Grid container justifyContent='space-around' alignItems='center'>
            <Grid>
                <ButtonBase sx={{ width: 500 }}>
                <img
                    src={props.activity.pictureUrl}
                    alt='activity'
                    width='100%'
                />
                </ButtonBase>
            </Grid>
            <Grid>
                <Grid justifyContent='space-around' alignItems='center' direction='row' style={{ textAlign: 'center' }}>
                    <Typography>{props.activity.hostName}</Typography>
                    <Typography>{props.activity.hostAddress}</Typography>
                </Grid>
                <Grid style={{ textAlign: 'center' }}>
                    <Button
                        variant='contained'
                        onClick={() => {
                            navigate(`/activity/${props.activity.hostId}/${props.activity.hostActivityId}`);
                        }}
                    >
                        Plus d'info
                    </Button>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default SearchList;