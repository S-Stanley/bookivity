import {
    Typography,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
    SelectChangeEvent,
    Button
} from "@mui/material";
import { useState } from "react";
import { useMutation, gql, useQuery } from "@apollo/client";

import './SearchForm.css'

const SearchForm = (props: { allActivitiesProposed: any }) => {

    const { data } = useQuery(
        gql`
            query getDataSearch {
                allActivities {
                    nodes {
                        id
                        name
                    }
                }
                allCities {
                    nodes {
                        name
                        id
                    }
                }
            }
        `
    );

    const [citySelected, setCitySelected] = useState<string>('');
    const [activitySelected, setActivitySelected] = useState<string>('');

    const [searchActivity] = useMutation(
        gql`
            mutation searchActivity (
                $_activityId: UUID!,
                $_cityId: UUID!,
            ) {
                searchActivity(
                    input: {
                        _activityId: $_activityId,
                        _cityId: $_cityId
                    }
                ) {
                    results {
                        hostId
                        hostName
                        hostAddress
                        pictureUrl
                        description
                        hostActivityId
                    }
                }
            }
        `
    )

    const handleSearch = async() => {
        const resultMutation = await searchActivity({
            variables: {
                _activityId: activitySelected,
                _cityId: citySelected
            }
        });
        props.allActivitiesProposed(resultMutation?.data?.searchActivity?.results);
    };

    return (
        <div>
            <div>
                <Typography className="title-select">Activité</Typography>
                <FormControl sx={{  minWidth: '100%' }}>
                    <InputLabel>Choisissez une activité</InputLabel>
                    <Select
                        value={activitySelected}
                        onChange={(event: SelectChangeEvent) => setActivitySelected(event?.target?.value)}
                        label="Choisissez une activité"
                    >
                        {(data?.allActivities?.nodes ?? []).map((activity: { id: string, name: string }) => {
                            return (
                                <MenuItem key={activity.id} value={activity.id}>{activity.name}</MenuItem>
                            )
                        })}
                    </Select>
                </FormControl>
            </div>
            <div>
                <Typography className="title-select">Ville</Typography>
                <FormControl sx={{  minWidth: '100%' }}>
                    <InputLabel>Choisissez une ville</InputLabel>
                    <Select
                        value={citySelected}
                        onChange={(event: SelectChangeEvent) => setCitySelected(event?.target?.value)}
                        label="Choisissez une ville"
                    >
                        {(data?.allCities?.nodes ?? []).map((city: { id: string, name: string }) => {
                            return (
                                <MenuItem key={city.id} value={city.id}>{city.name}</MenuItem>
                            )
                        })}
                    </Select>
                </FormControl>
            </div>
            <div className="button-submit-search" style={{ textAlign: 'center' }}>
                <Button
                    variant="contained"
                    onClick={() => handleSearch()}
                >
                    Valider
                </Button>
            </div>
        </div>
    )
};

export default SearchForm;