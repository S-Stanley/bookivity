import { Fragment, useState } from "react";

import SearchFormComponent from "./components/SearchForm";
import SearchList from "./components/SearchList";

import "./Search.css"

const Search = () => {

    const [activityList, setActivityList] = useState<{
        hostId: string,
        hostName: string,
        hostAddress: string
        pictureUrl: string,
        description: string,
        hostActivityId: string,
    }[]>([]);

    return (
        <div>
            <div className="form-search">
                <SearchFormComponent
                    allActivitiesProposed={(values: {
                        hostId: string,
                        hostName: string,
                        hostAddress: string
                        pictureUrl: string,
                        description: string,
                        hostActivityId: string,
                    }[]) => {
                        setActivityList(values);
                    }}
                />
            </div>
            <hr/>
            <div>
                {activityList.map((activity) => {
                    return (
                        <Fragment>
                            <SearchList
                                key={activity.hostId}
                                activity={activity}
                            />
                            <hr/>
                        </Fragment>
                    );
                })}
            </div>
        </div>
    )
};

export default Search;