import { GoogleLogin } from '@react-oauth/google';
import { useNavigate } from 'react-router-dom';
import jwt_decode from "jwt-decode";
import { gql, useMutation } from '@apollo/client';
import Cookies from 'universal-cookie';

const SignIn = () => {

    const cookies = new Cookies();

    const navigate = useNavigate();

    const [createOrGetUser] = useMutation(
        gql`
            mutation createOrGetUser (
                $userEmail: String!,
                $userFirstname: String!,
                $userFamilyname: String!
            ) {
                createOrGetUser(
                    input: {
                        userEmail: $userEmail,
                        userFirstname: $userFirstname,
                        userFamilyname: $userFamilyname
                    }
                ) {
                    user {
                        email
                        id
                        firstname
                        familyname
                        token
                    }
                }
            }
        `
    )

    return (
        <div>
            <GoogleLogin
                onSuccess={async(credentialResponse) => {
                    const info: { email: string, given_name: string, family_name: string } = jwt_decode(credentialResponse.credential ?? '');
                    const req = await createOrGetUser({
                        variables: {
                            userEmail: info?.email,
                            userFirstname: info?.given_name,
                            userFamilyname: info?.family_name
                        }
                    });
                    cookies.set('email', req?.data?.createOrGetUser?.user?.email);
                    cookies.set('token', req?.data?.createOrGetUser?.user?.token);
                    cookies.set('firstname', req?.data?.createOrGetUser?.user?.firstname);
                    cookies.set('familyname', req?.data?.createOrGetUser?.user?.familyname);
                    navigate('/search')
                }}
                onError={() => {
                    console.log('Login Failed');
                }}
                useOneTap
            />
        </div>
    )
};

export default SignIn;