import React from 'react';
import ReactDOM from 'react-dom/client';
import reportWebVitals from './reportWebVitals';

import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import { GoogleOAuthProvider } from '@react-oauth/google';
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';

import SignIn from './pages/SignIn';
import Search from './pages/search/Search';
import ActivityNew from './pages/activity/New';
import ActivityView from './pages/activity/View';
import HostView from './pages/hosts/View';

const root = ReactDOM.createRoot(
    document.getElementById('root') as HTMLElement
);

const client = new ApolloClient({
    uri: 'http://127.0.0.1:5433/graphql',
    cache: new InMemoryCache(),
});

const router = createBrowserRouter([
    {
        path: '/',
        element: <SignIn/>
    },
    {
        path: '/search',
        element: <Search/>
    },
    {
        path: '/activity/new',
        element: <ActivityNew/>
    },
    {
        path: '/activity/:host_id/:host_activity_id',
        element: <ActivityView/>
    },
    {
        path: '/host/:host_id',
        element: <HostView/>
    },
])

root.render(
    <React.StrictMode>
        <ApolloProvider client={client}>
            <GoogleOAuthProvider clientId={process.env.REACT_APP_GOOGLE_AUTH_CLIENT_ID ?? ''}>
                <RouterProvider router={router} />
            </GoogleOAuthProvider>
        </ApolloProvider>
    </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
